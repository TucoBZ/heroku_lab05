package br.edu.metrocamp.sistdistrib.heroku;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Product {
  private String name;
  private Double value;

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  @JsonProperty("price")
  public Double getValue() {
    return value;
  }

  public void setValue(Double value) {
    this.value = value;
  }

}
