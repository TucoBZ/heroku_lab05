package br.edu.metrocamp.sistdistrib.heroku;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.*;

@Controller
@SpringBootApplication
public class HerokuApplication {

	public static void main(String[] args) {
		SpringApplication.run(HerokuApplication.class, args);
	}

	@RequestMapping("/")
	@ResponseBody
	String home() {
		return "Olá Mundo!";
	}

	@RequestMapping("/Product")
	@ResponseBody
	Product getproduct() {
		Product product = new Product();
		product.setName("Parafuso");
		product.setValue(0.45);
		return product;
	}

	@RequestMapping("/ProductById")
	@ResponseBody
	Product getProductById(@RequestParam("id") Integer id) {
		HashMap<Integer, Product> products = new HashMap();
		Product product = new Product();
		product.setName("Parafuso");
		product.setValue(0.45);
		products.put(1,product);
		product = new Product();

		product.setName("Chave de Fenda");
		product.setValue(12.00);
		products.put(2,product);
		product = new Product();

		product.setName("Martelo");
		product.setValue(16.50);
		products.put(3,product);
		product = products.get(id);

		return product;
	}

}
